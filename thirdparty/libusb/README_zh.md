# libusb三方库说明
## 功能简介
  **libusb**是一个 C 库，提供对 USB 设备的通用访问。它旨在供开发人员用来促进与 USB 硬件通信的应用程序的生成

## 使用约束
- IDE版本：

  DevEco Studio 3.1 Release

  DevEco Studio 4.0.3.500

- SDK版本：

  ohos_sdk_public 4.0.8.1 (API Version 10 Release)

  ohos_sdk_public 4.0.10.7 (API Version 10 Release)

- 三方库版本：v1.0.26

- 当前适配的功能：支持与USB 硬件通信能力

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
